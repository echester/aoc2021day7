use strict;
use warnings;
use List::Util qw |min max|;

sub howMuchFuelForThisCrab($$) {
	my $d = max(@_) - min(@_);
	return $d * (1 + $d)/2;
}

my $infile = $ARGV[0];
die "\ninput file not specified; borking.\n\n" unless defined($infile);

my @crabs;
my @crabFuel;

open(my $inf, '<', $infile) || die "cannot open file $infile: $!\n";
while(<$inf>) { chomp; @crabs = split /,/; }
close $inf;

my $nearCrab = min(@crabs);
my $farCrab = max(@crabs);

for (my $p=$nearCrab; $p<=$farCrab; $p++) {
	foreach (@crabs) { $crabFuel[$p] += howMuchFuelForThisCrab($_, $p); }
}

print min(@crabFuel);
exit;
